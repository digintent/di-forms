<?php
use DI\Email;

	class EmailTest extends PHPUnit_Framework_TestCase
	{
		public function testEmailConstruct()
		{
			$config = include __DIR__.'/config/mandrill.php';
			$email = new Email($config['api']['key']);

			$this->assertNotNull($email->mailer);
		}

		public function testEmailSend()
		{
			$config = include __DIR__.'/config/mandrill.php';
			$email = new Email($config['api']['key']);

			$this->assertNotNull($email->mailer);

			$result = $email->send('nick@digintent.com', 'test@digintent.com', 'PHPUnit Test for diworks', 'Testing');

			$this->assertTrue($result);
		}

		public function testEmailSendTemplate()
		{
			$config = include __DIR__.'/config/mandrill.php';
			$email = new Email($config['api']['key'], $config['templates']);

			$this->assertNotNull($email->mailer);

			$result = $email->sendFromTemplate('nick@digintent.com', 'phpunit-test');

			$this->assertTrue($result);
		}
	}
