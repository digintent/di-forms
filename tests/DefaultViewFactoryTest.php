<?php

use diforms\DefaultViewFactory;

class DefaultViewFactoryTest extends PHPUnit_Framework_TestCase
{
	public function testGetInstance()
	{
		$factory = DefaultViewFactory::getInstance();
		$this->assertInstanceOf('diforms\DefaultViewFactory', $factory);
		$this->assertObjectHasAttribute('searchPaths', $factory);
		return $factory;
	}

	/**
	* @depends testGetInstance
	*/
	public function testGetSearchPaths(DefaultViewFactory $factory)
	{
		$paths = $factory->getSearchPaths();
		$this->assertInternalType('array', $paths);
		$this->assertCount(1, $paths);
		return $factory;
	}

	/**
	* @depends testGetSearchPaths
	*/
	public function testSetSearchPaths(DefaultViewFactory $factory)
	{
		$newPaths = ['/test/path/', '/second/test/path/'];
		$factory->setSearchPaths($newPaths);
		$paths = $factory->getSearchPaths();
		$this->assertInternalType('array', $paths);
		$this->assertCount(2, $paths);
		return $factory;
	}

	/**
	* @depends testSetSearchPaths
	*/
	public function testAddSearchPath(DefaultViewFactory $factory)
	{
		$factory->addSearchPath(__DIR__ . '/../views/fields/');
		$paths = $factory->getSearchPaths();
		$this->assertCount(3, $paths);
		return $factory;
	}

	/**
	* @depends testAddSearchPath
	*/
	public function testLoadView(DefaultViewFactory $factory)
	{
		$view = $factory->loadView('inexistent');
		$this->assertNull($view);
		$view = $factory->loadView('email');
		$this->assertNotNull($view);
		$this->assertInstanceOf('\diforms\View', $view);
	}
}
