<?php
namespace diforms;

use ReflectionObject;
use ReflectionClass;
use Exception;

Trait Getter
{
	protected $autoGetters = [];
	protected $__get_reflector;
	protected $__has_get;

	public function __get($name)
	{
		if (!$this->__get_reflector)
		{
			$this->__get_reflector = new ReflectionObject($this);
			$this->__has_get = $this->__get_reflector->hasMethod('get');
		}
		
		$reflector = $this->__get_reflector;

		if (in_array($name, $this->autoGetters))
		{
			return $this->$name;
		}

		$getter_name = "get$name";
		
		if ($reflector->hasMethod($getter_name))
		{
			$getter = [$this, $getter_name];
			return $getter();
		}
		else if ($this->__has_get) 
		{
			return $this->get($name);
		}
		else
		{
			$trait = new ReflectionClass(__CLASS__);
			$parent = $trait->getParentClass();
			
			if ($parent && $parent->hasMethod('__get'))
				return parent::__get($name);

			throw new Exception("Invalid property $name");
		}

	}
}