<?php namespace diforms;

/**
* Default view factory for di-form fields.
*/
class DefaultViewFactory implements ViewFactoryInterface
{
	private static $instance;

	private $searchPaths = [];

	private function __construct()
	{
		$this->searchPaths[] = __DIR__ . '/../views/fields/';
	}

	/**
	* Returns singleton Factory instance.
	*
	* @return DefaultViewFactory Factory instance.
	*/
	public static function getInstance()
	{
		if (!self::$instance)
		{
			self::$instance = new DefaultViewFactory();
		}

		return self::$instance;
	}

	/**
	* Returns a View object for the specified file.
	*
	* Iterates over the searchPaths and returns the view object if the filename is
	* found.
	*
	* @param string $filename Filename containing the view.
	* @return mixed \diforms\View object if filename found, null otherwise.
	*/
	public function loadView($filename)
	{
		foreach ($this->searchPaths as $path)
		{
			if (file_exists($path . $filename . '.php'))
			{
				return new View($path . $filename . '.php');
			}
		}
		return null;
	}

	/**
	* Sets searchPaths var with the array sent as parameter.
	*
	* @param array $paths Array containing the searchPaths.
	*/
	public function setSearchPaths(array $paths)
	{
		$this->searchPaths = $paths;
	}

	/**
	* Adds a new path to the searchPaths.
	*
	* @param string $path Search path.
	*/
	public function addSearchPath($path)
	{
		$this->searchPaths[] = $path;
	}

	/**
	* Returns the current searchPaths.
	*
	* @return array SearchPaths.
	*/
	public function getSearchPaths()
	{
		return $this->searchPaths;
	}
}
