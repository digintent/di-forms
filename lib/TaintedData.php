<?php namespace diforms;

use \mef\Validation\Untainted;
use \mef\Validation\UntaintedScalar;

class TaintedData
{
	/**
	 * Does the actual work of cleaning the variable. Objects and arrays
	 * are recursively iterated over. All string data types are encoded.
	 *
	 * @return void
	 * @author Matthew Leverton
	 **/
	public static function clean($val)
	{
		if (is_array($val))
		{
			foreach ($val as $key => $v)
				$val[$key] = self::clean($v);
		}
		else if (is_object($val))
		{
			$type = get_class($val);
			
			if ($type == 'stdClass')
			{
				foreach ($val as $key => $v)
					$val->$key = self::clean($v);
			}
			else if ($val instanceof Untainted)
			{
				# nothing to do
			}
			else if ($val instanceof Untaintable)
			{
				if ($val->isTainted())
					$val = $val->untaint();
			}
			else
			{
				$val = self::clean(get_object_vars($val));
			}
		}
		else if (is_string($val))
		{
			$e_val = htmlspecialchars($val);
			if ($e_val != $val)
				$val = new UntaintedScalar($e_val);
		}
		
		return $val;
	}
	
	/**
	 * Converts any scalar to a boolean
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_boolean($val)
	{
		if (!is_scalar($val)) return null;
		
		if (is_numeric($val)) return (boolean) $val;
		
		$val = strtolower($val);
		
		return $val == 'yes' || $val == 'true' || $val == 'on';
	}
	
	/**
	 * Converts any string with exactly nine numerical digits to a SSN.
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_ssn($val)
	{
		if (!is_scalar($val)) return null;
		
		$val = preg_replace('/[^\d]/', '', $val);
		return strlen($val) == 9 ? substr($val, 0, 3).'-'.substr($val,3,2).'-'.substr($val,5,4) : null;
	}
	
	/**
	 * Converts any string with at least ten numerical digits to a North American phone number.
	 * Extra digits are assumed to be the extension.
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_phone($val)
	{
		if (!is_scalar($val)) return null;
		
		$val = preg_replace('/[^\d]/', '', $val);
		if (strlen($val) < 10) return null;
		
		$number = '('.substr($val, 0, 3).') '.substr($val, 3, 3).'-'.substr($val, 6, 4);
		if (strlen($val) > 10) $number .= ' x'.substr($val, 10);
		
		return $number;
	}
	
	/**
	 * Converts any string with exactly five or nine numerical digits to a zip code.
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_zip($val)
	{
		if (!is_scalar($val)) return null;
		
		$val = preg_replace('/[^\d]/', '', $val);
		$len = strlen($val);
		
		if ($len == 5)
			return $val;
		else if ($len == 9)
			return substr($val, 0, 5).'-'.substr($val, 5, 4);
		
		return null;
	}
	
	/**
	 * Casts to a numeric string.
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_numeric($val)
	{
		if (!is_scalar($val) || !is_numeric($val)) return null;
		
		return $val;
	}
	
	/**
	 * Casts to an integer string by using all numeric digits at the
	 * beginning of the string. (An actual integer data type is not used
	 * to avoid problems with overflowing PHP's integer size.)
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_integer($val)
	{
		if (!is_scalar($val)) return null;
		
		if (!preg_match('/^(\d+)/', $val, $matches)) return null;
		
		return $matches[1];
	}
	
	/**
	 * Casts to a string.
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_string($val)
	{
		return is_scalar($val) ? (string) $val : null;
	}
	
	/**
	 * Casts to a string if it is part of the enumeration.
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_enum($val, $enum)
	{
		if (!is_scalar($val)) return null;
				
		return in_array($val, $enum) ? $val : null;
	}
	
	/**
	 * Validates the value as a $_FILE entry
	 *
	 * @return array
	 * @author Matthew Leverton
	 **/
	public static function cast_uploaded_file($val)
	{
		if (
			!is_array($val) ||
			!isset($val['tmp_name'], $val['error'], $val['name'], $val['size']) ||
			!is_uploaded_file($val['tmp_name'])
		) return null;
		
		return array(
			'tmp_name' => $val['tmp_name'],
			'error' => $val['error'],
			'name' => $val['name'], 
			'size' => $val['size']
		);
	}
	
	/**
	 * Ensures a format of a@b.c
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_email($val)
	{
		if (!is_scalar($val)) return null;
		
		return preg_match('/^[a-zA-Z0-9_\-\.]+@[a-z0-9.-]+(.[a-z]{2,})+$/i', $val) ? $val : null;
	}

	/**
	 * Returns a date in the format of YYYY-MM-DD HH:MM:SS
	 *
	 * @return string
	 * @author Matthew Leverton
	 **/
	public static function cast_datetime($val)
	{
		if (!trim($val)) return null;
		
		$dt = date_create($val);
		if (!$dt) return null;
		
		return $dt->format('Y-m-d H:i:s');
	}

	/**
	 * Returns a date in the format of YYYY-MM-DD
	 *
	 * @return string
	 **/
	public static function cast_date($val)
	{
		if (!trim($val)) return null;
		
		$dt = date_create($val);
		if (!$dt) return null;
		
		return $dt->format('Y-m-d');
	}
	
	/**
	* Returns a month in the format of MM
	* 
	* @return string
	**/
	public static function cast_month($val)
	{
       if (!trim($val)) return null;

       if ((int)$val < 1 || (int)$val > 12) return null;

       return $val;
	}

	/**
	* Returns a year in the format of YYYY
	* 
	* must be in the range of 1900 to 3000
	* 
	* @return string
	**/
	public static function cast_year($val)
	{
       if (!trim($val)) return null;

       if ((int)$val < 1900 || (int)$val > 3000) return null;

       return $val;
	}

	/**
	* Returns a day in the format of DD
	* 
	* must be in the range of 1 to 31
	* 
	* @return string
	**/
	public static function cast_day($val)
	{
       if (!trim($val)) return null;

       if ((int)$val < 1 || (int)$val > 31) return null;

       return $val;
	}

	/**
	* Returns a hour in the format of 24HH
	* 
	* must be in the range of 1 to 23
	* 
	* @return string
	**/
	public static function cast_hour($val)
	{
       if (!trim($val) && trim($val) != '0') return null;

       if ((int)$val < 0 || (int)$val > 23) return null;

       return $val;
	}

	/**
	* Returns a minute in the format of MM
	* 
	* must be in the range of 0 to 59
	* 
	* @return string
	**/
	public static function cast_minute($val)
	{
       if (!trim($val) && trim($val) != '0') return null;

       if ((int)$val < 0 || (int)$val > 59) return null;

       return $val;
	}

	/**
	* Returns a second in the format of SS
	* 
	* must be in the range of 0 to 59
	* 
	* @return string
	**/
	public static function cast_second($val)
	{
       if (!trim($val) && trim($val) != '0') return null;

       if ((int)$val < 0 || (int)$val > 59) return null;

       return $val;
   }

   /**
    * Casts to a string.
    *
    * @return string
    * @author Thomas Fjellstrom
    **/
   public static function cast_hidden($val)
   {
      return is_scalar($val) ? (string) $val : null;
   }

}
