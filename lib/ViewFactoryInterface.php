<?php namespace diforms;

/**
* Interface for View Factories 
*/
interface ViewFactoryInterface
{
	/**
	* Returns a View object for the specified file.
	*
	* @param string $filename Filename containing the view.
	* @return \diforms\View View object.
	*/
	public function loadView($filename);
}
