<?php namespace diforms;

class Email
{
	const ASYNC = true;

	protected $mailer;
	protected $templates;

	public function __construct($key, $templates = [])
	{
		$this->mailer = new \Mandrill($key);
		$this->templates = $templates;
	}

	public function send($to, $from, $subject, $body)
	{
		$params = new \stdClass();
		$params->to = [new \stdClass()];
		$params->to[0]->email = $to;
		$params->subject = $subject;
		$params->from_email = $from;
		$params->html = $body;
		$result = $this->mailer->messages->send($params, self::ASYNC);
		
		return $this->examineResults($result);
	}

	public function sendFromTemplate($to, $templateName, $content = [])
	{
		$params = new \stdClass();
		$params->to = [new \stdClass()];
		$params->to[0]->email = $to;

		if (!isset($this->templates[$templateName])) {
			throw new \Exception("Mail Template Not Found");
		}

		$template = $this->templates[$templateName];

		$params->subject = $template['subject'];
		$params->from_email = $template['from'];

		$params->global_merge_vars = [];
		$bindings = [];
		foreach($content as $key => $value)
		{
			$var = new \stdClass();
			$var->name = $key;
			$var->content = $value;
			$bindings[] = $var;
			$params->global_merge_vars[] = $var;
		}

		$result = $this->mailer->messages->sendTemplate($template['name'], $bindings, $params, self::ASYNC);

		return $this->examineResults($result);
	}

	protected function examineResults($result)
	{
		if (is_array($result) && isset($result[0]) && isset($result[0]['status']) && ( $result[0]['status'] == 'queued' || $result[0]['status'] == 'sent'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function __get($key)
	{
		switch ($key)
		{
			case 'templates':
				return $this->templatess;
			case 'mailer':
				return $this->mailer;
		}
		
		throw new Exception("Invalid property $key");
	}

	public function templates($templates)
	{
		$this->templates = $templates;
		return $this->templates;
	}
}