<?php namespace diforms;

class Field_Second extends Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'second';
           $this->type = 'second';
   }
}
