<?php namespace diforms;

class Field_Date extends Field_Text
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this['class'] = 'date';
		$this->type = 'date';
	}
}