<?php namespace diforms;

class Field_Phone extends Field_Text
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this['class'] = 'phone';
	}
}
