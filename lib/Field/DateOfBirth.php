<?php namespace diforms;

class Field_DateOfBirth extends Field_Date
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this['class'] = 'date dob';
	}
}