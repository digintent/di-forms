<?php namespace diforms;

class Field_Submit extends Field_Visible
{
	protected $caption;
	protected $defaultTemplate = 'submit';

	public function __construct($id, $attr = [])
	{
		$attr = array_merge(['caption' => null], $attr);
		parent::__construct($id, $attr);
		$this->autoGetters = array_merge($this->autoGetters, ['caption']);

		$this->caption = $attr['caption'];
	}
}