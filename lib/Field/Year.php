<?php namespace diforms;

class Field_Year extends Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'year';
           $this->type = 'year';
   }
}