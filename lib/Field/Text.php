<?php namespace diforms;

class Field_Text extends Field_Visible
{
	protected $type;
	protected $defaultTemplate = 'text';

	public function __construct($id, $attr = [])
	{
		$attr = array_merge(['type' => 'string'], $attr);
		parent::__construct($id, $attr);
		$this->type = $attr['type'];
	}

	protected function getType()
	{
		return $this->type;
	}

	protected function getSaneValue()
	{
		$filter = new InputFilter([$this->id => $this->value]);
		$filter->sanitize([$this->id => $this->type]);
		return $filter[$this->id];
	}
}
