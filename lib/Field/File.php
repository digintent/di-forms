<?php namespace diforms;

class Field_File extends Field_Visible
{
	protected $defaultTemplate = 'file';

	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
	}

	protected function getFileName()
	{
		return isset($this->value['name']) ? $this->value['name'] : null;
	}

	protected function getTempName()
	{
		return isset($this->value['tmp_name']) ? $this->value['tmp_name'] : null;
	}

	protected function getSaneValue()
	{
		return $this->value;
	}
}
