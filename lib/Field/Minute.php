<?php namespace diforms;

class Field_Minute extends Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'minute';
           $this->type = 'minute';
   }
}
