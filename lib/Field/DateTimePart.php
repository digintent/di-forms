<?php namespace diforms;

class Field_DateTimePart extends Field_Object
{
	protected $srcTimeZone;
	protected $destTimeZone;
	protected $defaultTemplate = 'datetimepart';

	public function __construct($id, $attr = [])
	{
		parent::__construct($id);

		parent::addField(new Field_Year('year'));
		parent::addField(new Field_Month('month'));
		parent::addField(new Field_Day('day'));
		parent::addField(new Field_Hour('hour'));
		parent::addField(new Field_Minute('minute'));
		parent::addField(new Field_Second('second'));

		$this->srcTimeZone = DateTimeZone::factory(
			isset($attr['srcTimeZone']) ? $attr['srcTimeZone'] : 'UTC' );

		$this->destTimeZone = DateTimeZone::factory(
			isset($attr['destTimeZone']) ? $attr['destTimeZone'] : 'UTC');
	}

	protected function setValue($value)
	{
		if (is_array($value))
		{
			parent::setValue($value);
		}
		else
		{
			if ($value == null)
			{
				parent::setValue([
					'year' => null,
					'month' => null,
					'day' => null,
					'hour' => null,
					'minute' => null,
					'second' => null
				]);
			}
			else
			{
				# If it is a string, assume that it is coming from the database in the
				# destination time zone, and needs to be converted to the source time zone.
				$dt = new \DateTime($value, $this->destTimeZone);
				$dt->setTimeZone($this->srcTimeZone);
				parent::setValue([
					'year' => $dt->format('Y'),
					'month' => $dt->format('m'),
					'day' => $dt->format('d'),
					'hour' => $dt->format('H'),
					'minute' => $dt->format('i'),
					'second' => $dt->format('s')
				]);
			}
		}
	}

	/**
	 * If the field is required, then both month/year are required.
	 */
	protected function setRequired($value)
	{
		$this['year']->required = $value;
		$this['month']->required = $value;
		$this['day']->required = $value;
		$this['hour']->required = $value;
		$this['minute']->required = $value;
		$this['second']->required = $value;
	}

	protected function getSaneValue()
	{
		$year = $this->fields['year']->saneValue;
		$month = $this->fields['month']->saneValue;
		$day = $this->fields['day']->saneValue;
		$hour = $this->fields['hour']->saneValue;
		$minute = $this->fields['minute']->saneValue;
		$second = $this->fields['second']->saneValue;

		if ($year === null || $month === null || $day === null || $hour === null || $minute === null || $second === null)
		{
			return null;
		}
		else
		{
			$dt = new \DateTime($year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':'.$second, $this->srcTimeZone);
			$dt->setTimeZone($this->destTimeZone);
			return $dt->format('Y-m-d H:i:s');
		}
	}

	public function addField(Field $field, $label = '')
	{
		throw new \Exception('Cannot add fields to DateTime');
	}
}