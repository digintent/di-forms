<?php namespace diforms;

class Field_Month extends Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'month';
           $this->type = 'month';
   }
}
