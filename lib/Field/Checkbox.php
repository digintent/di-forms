<?php namespace diforms;

class Field_Checkbox extends Field_Visible
{
	protected $caption;
	protected $defaultTemplate = 'checkbox';

	public function __construct($id, $attr = [])
	{
		$attr = array_merge(['caption' => ''], $attr);
		parent::__construct($id, $attr);
		$this->autoGetters = array_merge($this->autoGetters, ['caption']);

		$this->caption = $attr['caption'];
	}

	public function isChecked()
	{
		return $this->value != 0;
	}

	protected function getCaption()
	{
		return $this->caption;
	}
}