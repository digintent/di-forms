<?php namespace diforms;

class Field_Email extends Field_Text
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this->rules->add(new Rule_Email());
	}
}