<?php namespace diforms;

class Field_Day extends Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'day';
           $this->type = 'day';
   }
}
