<?php namespace diforms;

use \DateTime, \DateTimeZone, \Exception;

class Field_DateTime extends Field_Object
{
	protected $srcTimeZone;
	protected $destTimeZone;
	protected $defaultTemplate = 'group';

	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);

		parent::addField(new Field_Date('date'));
		parent::addField(new Field_Time('time'));

		$this->srcTimeZone = DateTimeZone::factory(
			isset($attr['srcTimeZone']) ? $attr['srcTimeZone'] :
			date_default_timezone_get()
		);

		$this->destTimeZone = DateTimeZone::factory(
			isset($attr['destTimeZone']) ? $attr['destTimeZone'] : 'UTC'
		);
	}

	protected function setValue($value)
	{
		if (is_array($value))
		{
			parent::setValue($value);
		}
		else
		{
			# If it is a string, assume that it is coming from the database in the
			# destination time zone, and needs to be converted to the source time zone.
			$dt = new DateTime($value, $this->destTimeZone);
			$dt->setTimeZone($this->srcTimeZone);
			parent::setValue([
				'date' => $dt->format('m/d/Y'),
				'time' => $dt->format('h:i:s A')
			]);
		}
	}

	protected function getSaneValue()
	{
		$date = $this->fields['date']->saneValue;
		$time = $this->fields['time']->saneValue;

		if ($date === null || $time === null)
		{
			return null;
		}
		else
		{
			$dt = new DateTime($date.' '.$time, $this->srcTimeZone);
			$dt->setTimeZone($this->destTimeZone);
			return $dt->format('Y-m-d H:i:s');
		}
	}

	public function addField(Field $field, $label = '')
	{
		throw new Exception('Cannot add fields to DateTime');
	}
}