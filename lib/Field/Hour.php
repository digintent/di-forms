<?php namespace diforms;


class Field_Hour extends Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'hour';
           $this->type = 'hour';
   }
}
