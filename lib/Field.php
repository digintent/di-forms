<?php namespace diforms;

use \mef\MVC\PHPView as View;
use \mef\Validation\UntaintedScalar;
use \mef\Validation\Untainted;

abstract class Field implements Untainted
{
	use \diforms\Getter, \diforms\Setter;

	protected $id;
	protected $label;
	protected $value;
	protected $type;
	protected $requiredRule;

	protected $parent;

	protected $errors = [];
	protected $rules;

	protected $defaultTemplate = 'default';
	protected $htmlAttributes = [];

	private $viewFactory;

	public function __construct($id, $attr = [])
	{
		$attr = array_merge([
			'type' => 'string',
			'label' => null,
			'value' => null
		], $attr);

		$this->id = $id;
		$this->type = $attr['type'];
		$this->label = $attr['label'];
		$this->value = $attr['value'];
		$this->rules = new RuleCollection();

		if (isset($attr['html']))
		{
			$this->htmlAttributes = $attr['html'];
		}

		$this->viewFactory = DefaultViewFactory::getInstance();
	}

	protected function getLabel()
	{
		return $this->label;
	}

	protected function getId()
	{
		return $this->id;
	}

	protected function getType()
	{
		return $this->type;
	}

	protected function getValue()
	{
		return $this->value;
	}

	protected function getErrors()
	{
		return $this->errors;
	}

	protected function getFullyQualifiedId()
	{
		return strtr(($this->parent ? $this->parent->transformName($this, $this->id) : $this->id), ['[' => '_', ']' => '']);
	}

	protected function getHTML()
	{
		ob_start();
		$this->render();
		return ob_get_clean();
	}

	protected function getRequired()
	{
		return $this->requiredRule != null;
	}

	protected function getRules()
	{
		return $this->rules;
	}

	protected function setRequired($required)
	{
		if ($required == $this->required)
		{
			# do nothing
		}
		else if ($required)
		{
			$this->requiredRule = new Rule_RequiredField($this);
			$this->rules->add($this->requiredRule);
		}
		else
		{
			$this->rules->remove($this->requiredRule);
			$this->requiredRule = null;
		}
	}

	protected function getSaneValue()
	{
		return trim((string) $this->getValue());
	}

	protected function setValue($value)
	{
		$this->value = $value;
	}

	protected function getParent()
	{
		return $this->parent;
	}

	protected function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * Returns an array of exported rules.
	 *
	 * @return array [['fieldId' => string, 'rule' => DI_Rule], ... ]
	 */
	public function exportRules()
	{
		$id = $this->fullyQualifiedId;
		$rules = [];
		foreach ($this->rules as $rule)
		{
			$rules[] = ['fieldId' => $id, 'rule' => $rule->asArray()];
		}

		return $rules;
	}

	public function validate()
	{
		$valid = true;

		foreach ($this->rules as $rule)
		{
			$result = $rule->exec($this->getSaneValue());
			if ($result->code)
			{
				$result['field'] = $this;
				$this->errors[] = $result;
				$valid = false;
			}
		}

		return $valid;
	}

	public function render($attributes = [])
	{
		$template = isset($attributes[':template']) ? $attributes[':template'] : $this->defaultTemplate;

		$tmp_attributes = $this->htmlAttributes;

		$this->htmlAttributes = array_merge($this->htmlAttributes, $attributes);

		$html = $this->viewFactory->loadView($template);
		$html->set('attributes', $this->htmlAttributes);
		$html->set('field', $this);

		if (isset($attributes[':html']))
		{
			$html = new UntaintedScalar(str_replace('{field}', $html, $attributes[':html']));
		}

		$html2 = $this->viewFactory->loadView('field');

		$html2->set('field', $this);
		$html2->set('attributes', $this->htmlAttributes);
		$html2->set('html', $html);
		echo $html2;

		$this->htmlAttributes = $tmp_attributes;
	}

	public function getViewFactory()
	{
		return $this->viewFactory;
	}

	public function setViewFactory($viewFactory)
	{
		$this->viewFactory = $viewFactory;
	}
}
