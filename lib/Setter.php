<?php
namespace diforms;

use ReflectionClass;
use Exception;

Trait Setter
{
    protected $__set_reflector;
    protected $__has_set;

    public function __set($name, $value)
    {
        if (!$this->__set_reflector)
        {
            $this->__set_reflector = new ReflectionClass($this);
            $this->__has_set = $this->__set_reflector->hasMethod('set');
        }

        $reflector = $this->__set_reflector;

        $setter_name = "set$name";
        
        if ($reflector->hasMethod($setter_name))
        {
            $setter = [$this, $setter_name];
            return $setter($value);
        }
        else if ($this->__has_set)
        {
            return $this->set($name, $value);
        }
        else
        {
            $trait = new ReflectionClass(__CLASS__);
            $parent = $trait->getParentClass();
            
            if ($parent && $parent->hasMethod('__set'))
                return parent::__set($name, $value);
                
            throw new Exception("Invalid property $name");
        }
        
    }
}