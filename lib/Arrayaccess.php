<?php
namespace diforms;

trait ArrayAccess
{
	protected $_arrayData = [];

	public function offsetExists($key)
	{
		return array_key_exists($key);
	}

	public function offsetGet($key)
	{
		return $this->_arrayData[$key];
	}

	public function offsetSet($key, $val)
	{
		$this->_arrayData[$key] = $val;
	}

	public function offsetUnset($key)
	{
		unset($this->_arrayData[$key]);
	}
}