<?php namespace diforms;

class InputFilter implements \ArrayAccess
{
	private $data, $unfiltered_data;
	private $aliases = array();
	private $required = array();
	
	private $rules = array();
	private $errors, $bad_fields;
	
	public function __construct(array $data)
	{
		$this->data = $this->unfiltered_data = $data;
	}
	
	public function __get($key)
	{
		switch ($key)
		{
			case 'errors': return $this->errors;
			case 'bad_fields': return $this->bad_fields;
		}
		
		throw new \Exception("Invalid property $key");
	}
	
	public function offsetExists($key)
	{
		return array_key_exists($key, $this->data);
	}
	
	public function offsetGet($key)
	{
		return $this->data[$key];
	}
	
	public function offsetSet($key, $val)
	{
		$this->data[$key] = $val;
	}
	
	public function offsetUnset($key)
	{
		unset($this->data[$key]);
	}
	
	public function compose($name, $format)
	{
		$args = array();
		
		foreach (array_slice(func_get_args(), 2) as $key)
		{
			if (!array_key_exists($key, $this->data)) throw new \Exception("$key not found in data");
			
			$args[] = $this->data[$key];
		}
		
		$this->data[$name] = vsprintf($format,  $args);
		
		return $this;
	}
	
	public function alias($aliases)
	{
		$this->aliases = array_merge($this->aliases, $aliases);
		
		return $this;
	}
	
	public function required()
	{
		$this->required = array_unique(array_merge($this->required, func_get_args()));
				
		return $this;
	}
	
	public function rule($rule)
	{
		$this->rules[] = $rule;
		return $this;
	}
	
	public function check()
	{
		$ok = true;
		$errors = array();
		$fields = array();
		
		foreach ($this->rules as $rule)
		{
			$resp = $rule($this);
			if (is_array($resp))
			{
				$errors = array_merge($errors, $resp['errors']);
				$fields = array_unique(array_merge($fields, $resp['fields']));
				$ok = false;
			}
		}
		
		$this->errors = $errors;
		$this->bad_fields = $fields;
		
		return $ok;
	}
	
	public function sanitize($types)
	{
		$good_keys = array_keys($types);
		$data_keys = array_keys($this->data);
		
		foreach (array_diff($data_keys, $good_keys) as $key)
			unset($this->data[$key]);
		
		foreach (array_diff($good_keys, $data_keys) as $key)
			$this->data[$key] = null;
			
		$data = &$this->data;
			
		array_walk_recursive($data, 'trim');
		
		foreach ($types as $key => $val)
		{
			if (array_key_exists($key, $data))
			{
				if (is_string($val))
				{
					if (substr($val, -2) != '[]')
						$multiple = false;
					else
					{
						$val = substr($val, 0, -2);
						$multiple = true;
					}
					
					$rule = array('multiple' => $multiple, 'type' => $val);					
				}
				else if (is_array($val))
				{
					if (isset($val[0]))
					{
						$rule = array('type' => 'enum', 'params' => $val);
					}
					else
					{
						$rule = $val;
					}
				}
				else
					throw new \Exception("Unexpected value");
					
				if (isset($rule['type']))
					$rule['callback'] = '\diforms\TaintedData::cast_'.$rule['type'];
				
				if (!isset($rule['params']))
					$rule['params'] = null;
				
				if (isset($rule['multiple']) && $rule['multiple'] == true)
				{
					if (!is_array($data[$key]))
					{
						# The input data wasn't an array, so no sense in even validating it.
						$data[$key] = array();
					}
					else
					{
						if (isset($rule['unique']) && $rule['unique'] == true)
							$data[$key] = array_unique($data[$key]);
							
						foreach ($data[$key] as $sub_key => $sub_val)
						{
							$data[$key][$sub_key] = call_user_func($rule['callback'], $data[$key][$sub_key], $rule['params']);
						}
					}
					
				}
				else
				{
					$data[$key] = call_user_func($rule['callback'], $data[$key], $rule['params']);
				}
			}
		}
		
		return $this;
	}
	
	public function get_field_name($key)
	{
		return isset($this->aliases[$key]) ? $this->aliases[$key] : $key;
	}
	
	public function as_array()
	{
		return $this->data;
	}
	
	public function as_form_data()
	{
		$data = array();
		foreach ($this->data as $key => $val)
		{
			$data[$key] = ($val !== null || !array_key_exists($key, $this->unfiltered_data)) ? $val : $this->unfiltered_data[$key];
		}
		
		return $data;
	}
}