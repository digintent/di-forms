<?php namespace diforms;

interface Untaintable
{
	public function untaint();
	public function isTainted();
}
