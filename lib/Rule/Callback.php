<?php namespace diforms;

class Rule_Callback extends Rule
{
	protected $callback;

	public function __construct(callable $callback)
	{
		$this->callback = $callback;
	}

	public function bindTo($obj)
	{
		$this->callback = $this->callback->bindTo($obj);
		return $this;
	}

	public function exec()
	{
		return call_user_func_array($this->callback, func_get_args());
	}
}