<?php namespace diforms;

class Rule_StrongPassword extends Rule_Value
{
	protected function execValue($value)
	{
		return (strlen($value) < 6) ? new RuleResult_Failure('weak-password') : new RuleResult_Success();

	}
}