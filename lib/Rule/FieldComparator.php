<?php namespace diforms;

class Rule_FieldComparator extends Rule_Value
{
	protected $field, $comparison;

	public function __construct(Field $field, $comparison)
	{
		$this->field = $field;
		$this->comparison = $comparison;
	}

	protected function execValue($value)
	{
		$compareTo = $this->field->saneValue;

		switch ($this->comparison)
		{
			case '<':
				$error = $value >= $compareTo;
			break;

			case '<=':
				$error = $value > $compareTo;
			break;

			case '==':
				$error = $value != $compareTo;
			break;

			case '>':
				$error = $value <= $compareTo;
			break;

			case '>=':
				$error = $value < $compareTo;
			break;

			default:
				$error = false;
		}

		return $error ? new RuleResult_Failure('comparison') : new RuleResult_Success();
	}

	public function asArray()
	{
		return [
			'name' => 'DI.FieldComparator',
			'field' => $this->field->fullyQualifiedId,
			'comparison' => $this->comparison
		];
	}
}