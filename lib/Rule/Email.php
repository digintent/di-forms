<?php namespace diforms;

class Rule_Email extends Rule_Value
{
	function execValue($data)
	{
		if($data)
		{
			// First, we check that there's one @ symbol, 
			// and that the lengths are right.
			if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/i", $data))
			{
				// Email invalid because wrong number of characters 
				// in one section or wrong number of @ symbols.
				return new RuleResult_Failure('Not a valid e-mail address');
			}
			// Split it into sections to make it easier
			$email_array = explode("@", $data);
			$local_array = explode(".", $email_array[0]);
			for ($i = 0; $i < sizeof($local_array); $i++)
			{
				if(!preg_match("/^[[:alnum:]][+a-z0-9_.-]*$/i", $local_array[$i]))
				{
					return new RuleResult_Failure('Not a valid e-mail address');
				}
			}
			// Check if domain is IP. If not, 
			// it should be valid domain name
			if (!preg_match("/^\[?[0-9\.]+\]?$/i", $email_array[1]))
			{
				$domain_array = explode(".", $email_array[1]);
				if (sizeof($domain_array) < 2)
				{
					// Not enough parts to domain
					return new RuleResult_Failure('Not a valid e-mail address');
				}
				for ($i = 0; $i < sizeof($domain_array); $i++)
				{
					if(!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/i", $domain_array[$i]))
					{
						return new RuleResult_Failure('Not a valid e-mail address');
					}
				}
			}
			return new RuleResult_Success();
		}
		return new RuleResult_Success();
	}
}