<?php namespace diforms;

class Rule_ZipCode extends Rule_Value
{
	function execValue($data)
	{
		if($data)
		{
			if (preg_match("/^[0-9]{5}$/", $data))
			{
				// a valid 5 digit number
				return new RuleResult_Success();
			}
			else
			{
				// not a valid 5 digit number
				return new RuleResult_Failure('Not a valid 5 digit Zip Code');
			}
			return new RuleResult_Success();
		}
		return new RuleResult_Success();
	}
}