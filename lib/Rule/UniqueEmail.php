<?php namespace diforms;

class Rule_UniqueEmail extends Rule_Value
{
	protected function execValue($data)
	{
		$person = Model::factory('person')->where('email', '=', $this['email']->value)->find();
		return $person->loaded() ? new RuleResult_Failure('Email already exists.') : new RuleResult_Success();
	}
}