<?php namespace diforms;

class Rule_RequiredField extends Rule_Value
{
	protected $field;

	public function __construct(Field $field)
	{
		$this->field = $field;
		parent::__construct();
	}

	protected function execValue($value)
	{
		if ($this->field instanceof Field_Checkbox)
			$missing = ($value != '1');
		else
			$missing = ($value === '' || $value === null);

		return $missing ?
			new RuleResult_Failure('missing') : new RuleResult_Success();
	}
}