<?php namespace diforms;

class Rule_NotEmpty extends Rule_Value
{
	protected function execValue($data)
	{
		return ($data === '' || $data === null) ?
			new RuleResult_Failure('Cannot be empty') : new RuleResult_Success();
	}
}