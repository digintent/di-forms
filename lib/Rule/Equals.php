<?php namespace diforms;

class Rule_Equals extends Rule_Value
{
	protected $value;

	public function __construct($value)
	{
		$this->value = $value;
	}

	protected function execValue($data)
	{
		return $data !== $this->value ?
			new RuleResult_Failure('Does not equal value') : new RuleResult_Success();
	}
}