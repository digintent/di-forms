<?php namespace diforms;


class RuleResult implements \ArrayAccess, Untaintable
{
	use \diforms\Getter, \diforms\Setter;
	use \diforms\ArrayAccess;

	protected $code;
	protected $error_messages;

	public function __construct($code = null, $error_messages = null)
	{
		$this->code = $code;
		$this->error_messages = [
			'missing' => 'Required field',
			'out-of-range' => 'Out of range',
			'weak-password' => 'The password is too weak',
			'luhn-checksum' => 'Invalid number'
		];
	}

	protected function getCode()
	{
		return $this->code;
	}

	protected function setCode($code)
	{
		$this->code = $value;
	}

	protected function getMessage()
	{
		$message = null;
		if (isset($this->error_messages[$this->code]))
		{
			$message = $this->error_messages[$this->code];
		}

		return $message ?: $this->code;
	}
	
	public function isTainted()
	{
		return true;
	}

	public function untaint()
	{
		return TaintedData::clean(array_merge(
			$this->_arrayData, [
				'code' => $this->code,
				'message' => $this->message
			]
		));		
	}

	public function __toString()
	{
		return (string) $this->message;
	}
}
