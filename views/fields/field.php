<div class="field<?php if ($field->errors): ?> error<?php endif ?>">
	<?= $html ?>

<?php if ($field->errors): ?>
<span class="error">
<?php foreach ($field->errors as $error): ?>

<?php if (isset($attributes[':errors'][$error->code])): ?>
<?= $attributes[':errors'][$error->code] ?>
<?php else: ?>
<?= $error ?>
<?php endif ?>

<?php endforeach ?>
</span>
<?php endif ?>

</div>
