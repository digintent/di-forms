<span <?php $field->renderHTMLAttributes() ?>>

<input
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="hidden"
	value="0"
/>

<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="checkbox"
	value="1"
<?php if ($field->value != 0): ?>
	checked="checked"
<?php endif ?>
/>

<?php $caption = isset($attributes[':caption']) ? $attributes[':caption'] : $field->caption ?>

<?php if ($caption != ''): ?>
<label
	for="<?= htmlspecialchars($field->fullyQualifiedId) ?>">
	<?= \diforms\TaintedData::clean($caption) ?>
</label>
<?php endif ?>

</span>
