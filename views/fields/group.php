<fieldset id="<?= htmlspecialchars($field->fullyQualifiedId) ?>">
<?php $i = 0; $lb = isset($attributes[':lineBreaks']) ? $attributes[':lineBreaks'] : 0; ?>
<?php foreach ($field->fields as $f): ?>
<?php $f->render(); ?>
<?php if ($lb && !(++$i % $lb)): ?><br /><?php endif ?>
<?php endforeach ?>
</fieldset>
