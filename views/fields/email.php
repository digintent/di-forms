<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName)?>"
	type="email"
	value="<?= htmlspecialchars($field->value) ?>"
	<?php $field->renderHTMLAttributes(); ?>
/>