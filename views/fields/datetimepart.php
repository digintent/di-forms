<input
	id="<?= htmlspecialchars($field['month']->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field['month']->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field['month']->value) ?>"
	<?php if (isset($attributes['month'])): ?>
	<?php foreach ($attributes['month'] as $key => $val): ?>
	<?php if ($key[0] != ':') echo ' ', $key, '="', htmlspecialchars($val), '"'; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	placeholder="MM"
	maxlength=2
/><span>/</span>
<input
	id="<?= htmlspecialchars($field['day']->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field['day']->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field['day']->value) ?>"
	<?php if (isset($attributes['day'])): ?>
	<?php foreach ($attributes['day'] as $key => $val): ?>
	<?php if ($key[0] != ':') echo ' ', $key, '="', htmlspecialchars($val), '"'; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	placeholder="DD"
	maxlength=2
/><span>,</span>
<input
	id="<?= htmlspecialchars($field['year']->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field['year']->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field['year']->value) ?>"
	<?php if (isset($attributes['year'])): ?>
	<?php foreach ($attributes['year'] as $key => $val): ?>
	<?php if ($key[0] != ':') echo ' ', $key, '="', htmlspecialchars($val), '"'; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	placeholder="YYYY"
	maxlength=4
/>
<span>@</span>
<input
	id="<?= htmlspecialchars($field['hour']->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field['hour']->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field['hour']->value) ?>"
	<?php if (isset($attributes['hour'])): ?>
	<?php foreach ($attributes['hour'] as $key => $val): ?>
	<?php if ($key[0] != ':') echo ' ', $key, '="', htmlspecialchars($val), '"'; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	placeholder="HH"
	maxlength=2
/><span>:</span>
<input
	id="<?= htmlspecialchars($field['minute']->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field['minute']->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field['minute']->value) ?>"
	<?php if (isset($attributes['minute'])): ?>
	<?php foreach ($attributes['minute'] as $key => $val): ?>
	<?php if ($key[0] != ':') echo ' ', $key, '="', htmlspecialchars($val), '"'; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	placeholder="MM"
	maxlength=2
/><span>:</span>
<input
	id="<?= htmlspecialchars($field['second']->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field['second']->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field['second']->value) ?>"
	<?php if (isset($attributes['second'])): ?>
	<?php foreach ($attributes['second'] as $key => $val): ?>
	<?php if ($key[0] != ':') echo ' ', $key, '="', htmlspecialchars($val), '"'; ?>
	<?php endforeach; ?>
	<?php endif; ?>
	placeholder="SS"
	maxlength=2
/>