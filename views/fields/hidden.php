<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName)?>"
	type="hidden"
	value="<?= htmlspecialchars($field->value) ?>"
	<?php $field->renderHTMLAttributes(); ?>
/>

