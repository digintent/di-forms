<select
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
<?php $field->renderHTMLAttributes() ?>
>

<?php if ($field->nullItem !== null): ?>
<option value=""><?= htmlspecialchars($field->nullItem) ?></option>
<?php endif ?>

<?php foreach ($field->options as $value => $caption): ?>
<option value="<?= htmlspecialchars($value) ?>"
<?php if ($field->value !== null && $field->value == $value): ?>selected="selected"<?php endif ?>
/>
<?= $caption ?>
</option>
<?php endforeach ?>

</select>