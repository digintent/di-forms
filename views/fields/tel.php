<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName)?>"
	type="tel"
	value="<?= htmlspecialchars($field->value) ?>"
	<?php $field->renderHTMLAttributes(); ?>
/>