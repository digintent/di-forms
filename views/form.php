
<form action="<?= $form->action ?>" method="post" enctype="multipart/form-data">

<?php $form->renderRules() ?>

<?php if ($form->errors): ?>
<ul class="error">
<?php foreach ($form->errors as $error): ?>
	<li><?= htmlspecialchars($error) ?>.</li>
<?php endforeach ?>
</ul>
<?php endif ?>

<table>
<?php foreach($form->fields as $field): ?>

	<?php
		$label = ucwords(str_replace('_', ' ', $field->id));

		if ($field->label)
		{
			$label = $field->label;
		}

		if (isset($attributes[':labels'][$field->id]))
		{
			$label = $attributes[':labels'][$field->id];
		}
	?>

	<tr>
		<td><?= $label ?></td>
		<td><?= $field->html ?></td>
	</tr>
<?php endforeach ?>
</table>

</form>